package oop;

import oop.classes.Accessories;
import oop.classes.BouquetCreate;
import oop.classes.Flower;
import oop.enums.ColorOfFlowers;
import oop.enums.TypeOfFlowers;

import java.io.IOException;

public class Main {
      private static final BouquetCreate BOUQUET_CREATE = new BouquetCreate("Olha Sych");

    public static void main(String[] args) throws IOException {
        Flower flower1 = new Flower(TypeOfFlowers.ROSE, ColorOfFlowers.RED,
                50.00, 19.50,1);
        Flower flower2 = new Flower(TypeOfFlowers.SUNFLOWER, ColorOfFlowers.YELLOW,
                52.00, 24.50,1);
        Flower flower3 = new Flower(TypeOfFlowers.ORCHID, ColorOfFlowers.WHITE,
                43.50, 96.00,2);
        Flower flower4 = new Flower(TypeOfFlowers.CALENDULA, ColorOfFlowers.YELLOW,
                45.50, 23.00,3);
        Flower flower5 = new Flower(TypeOfFlowers.TULIP, ColorOfFlowers.ORANGE,
                45.00, 15.40,2);
        Flower flower6 = new Flower(TypeOfFlowers.CROCUS, ColorOfFlowers.BLUE,
                38.00, 27.00,2);
        Flower flower7 = new Flower(TypeOfFlowers.IRIS, ColorOfFlowers.WHITE,
                61.00, 46.80,1);
        Flower flower8 = new Flower(TypeOfFlowers.LILY, ColorOfFlowers.VIOLET,
                35.50, 35.00,2);
        BOUQUET_CREATE.getFlowers().add(flower1);
        BOUQUET_CREATE.getFlowers().add(flower2);
        BOUQUET_CREATE.getFlowers().add(flower3);
        BOUQUET_CREATE.getFlowers().add(flower4);
        BOUQUET_CREATE.getFlowers().add(flower5);
        BOUQUET_CREATE.getFlowers().add(flower6);
        BOUQUET_CREATE.getFlowers().add(flower7);
        BOUQUET_CREATE.getFlowers().add(flower8);
        Accessories accessor1=new Accessories("tape", "red", 5.0);
        Accessories accessor2=new Accessories("basket", "white", 35.0);
        Accessories accessor3=new Accessories("bow", "pink", 9.50);
        BOUQUET_CREATE.getAccessories().add(accessor1);
        BOUQUET_CREATE.getAccessories().add(accessor2);
        BOUQUET_CREATE.getAccessories().add(accessor3);
        BOUQUET_CREATE.executeStatement();
    }
}
