package oop.interfaces;

import oop.classes.BouquetOfFlowers;

import java.io.IOException;

public interface BouquetOfFlowersInterface {
    BouquetOfFlowers createBouquet() throws IOException;
}
