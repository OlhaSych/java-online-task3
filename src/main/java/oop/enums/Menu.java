package oop.enums;

public enum Menu {
    SHOW_FLOWERS(1, "Enter 1 if you want to see all flowers"),
    SHOW_ACCESSORIES(2, "Enter 2 if you want to see list of accessories"),
    CREATE_BOUQUET(3, "Enter 3 if you want to create a bouquet"),
    FIND_BY_LENGTH(4, "Enter 4 if you want to find flowers by length"),
    EXIT(5, "Enter 5 to exit");
    private final Integer index;
    private final String message;

    Menu(Integer index, String message) {
        this.index = index;
        this.message = message;
    }

    public Integer getIndex() {
        return index;
    }

    public String getMessage() {
        return message;
    }

    public static void showMenu() {
        System.out.println("Please, make your choice:");
        for (Menu menu : Menu.values()
        ) {
            System.out.println(menu.getIndex() + ":" + menu.getMessage());
        }
    }

    public static Menu getIndexOfChoice(Integer index) {
        for (Menu menu : Menu.values()
        ) {
            if (menu.getIndex().equals(index)) {
                return menu;
            }
        }
        return null;
    }
}
