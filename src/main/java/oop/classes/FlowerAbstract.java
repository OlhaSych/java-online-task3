package oop.classes;

import oop.enums.TypeOfFlowers;
import oop.interfaces.FlowerInterface;

public abstract class FlowerAbstract implements FlowerInterface {
    private TypeOfFlowers typeOfFlowers;

    public FlowerAbstract(TypeOfFlowers typeOfFlowers) {
        this.typeOfFlowers = typeOfFlowers;
    }

    public TypeOfFlowers getTypeOfFlowers() {
        return typeOfFlowers;
    }

    public void setTypeOfFlowers(TypeOfFlowers typeOfFlowers) {
        this.typeOfFlowers = typeOfFlowers;
    }

    @Override
    public String toString() {
        return "FlowerAbstract{" +
                "typeOfFlowers=" + typeOfFlowers +
                '}';
    }
}
