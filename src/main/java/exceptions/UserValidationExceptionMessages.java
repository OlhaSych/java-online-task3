package exceptions;

public enum UserValidationExceptionMessages {
    BAD_EMAIL(" You entered a bad email"), BAD_PASSWORD(" You entered a bad password");
    private final String message;

    UserValidationExceptionMessages(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }
}
