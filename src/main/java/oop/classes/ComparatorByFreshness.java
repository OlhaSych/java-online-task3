package oop.classes;

import java.util.Comparator;

public class ComparatorByFreshness implements Comparator<Flower> {
    public int compare(Flower o1, Flower o2) {
        return o1.getLevelOfFreshness().compareTo(o2.getLevelOfFreshness());
    }
}
