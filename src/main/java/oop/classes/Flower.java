package oop.classes;

import oop.enums.ColorOfFlowers;
import oop.enums.TypeOfFlowers;

import java.util.List;

public class Flower extends FlowerAbstract {

    private ColorOfFlowers colorOfFlower;
    private Double lengthOfStem;
    private Double price;
    private Integer levelOfFreshness;

    public Flower(TypeOfFlowers typeOfFlowers, ColorOfFlowers colorOfFlower,
                  Double lengthOfStem, Double price, Integer levelOfFreshness) {
        super(typeOfFlowers);
        this.colorOfFlower = colorOfFlower;
        this.lengthOfStem = lengthOfStem;
        this.price = price;
        this.levelOfFreshness = levelOfFreshness;
    }

    public ColorOfFlowers getColorOfFlower() {
        return colorOfFlower;
    }

    public void setColorOfFlower(ColorOfFlowers colorOfFlower) {
        this.colorOfFlower = colorOfFlower;
    }

    public Double getLengthOfStem() {
        return lengthOfStem;
    }

    public void setLengthOfStem(Double lengthOfStem) {
        this.lengthOfStem = lengthOfStem;
    }

    public Double getPrice() {
        return price;
    }

    public Integer getLevelOfFreshness() {
        return levelOfFreshness;
    }

    public void setLevelOfFreshness(Integer levelOfFreshness) {
        this.levelOfFreshness = levelOfFreshness;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public List<Flower> getAllFlowersByPrice(double minPrice, double maxPrice) {

        return null;
    }

    public void cutFlower() {
        System.out.println("This flower was cut to create a new bouquet!");
    }

    @Override
    public String toString() {
        return "Flower{" + super.getTypeOfFlowers() + ", " +
                colorOfFlower + "," +
                "length :" + lengthOfStem +
                ", price :" + price +
                ", day before cut :" + levelOfFreshness +
                '}';
    }
}
