package oop.classes;

import oop.enums.Menu;
import oop.interfaces.BouquetOfFlowersInterface;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class BouquetCreate implements BouquetOfFlowersInterface {
    private String authorOfBouquet;
    private final List<Flower> flowers = new ArrayList<Flower>();
    private final List<Flower> flowersOFBouquet = new ArrayList<Flower>();
    private final List<Accessories> accessories = new ArrayList<Accessories>();
    private final List<Accessories> accessoriesOfBouquet = new ArrayList<Accessories>();
    private Double totalPriceOfBouquet;
    private static final BufferedReader READER = new BufferedReader(new InputStreamReader(System.in));


    public BouquetCreate(String authorOfBouquet) {
        this.authorOfBouquet = authorOfBouquet;
    }

    public String getAuthorOfBouquet() {
        return authorOfBouquet;
    }

    public void setAuthorOfBouquet(String authorOfBouquet) {
        this.authorOfBouquet = authorOfBouquet;
    }

    public Double getTotalPriceOfBouquet() {
        return totalPriceOfBouquet;
    }

    public void setTotalPriceOfBouquet(Double totalPriceOfBouquet) {
        this.totalPriceOfBouquet = totalPriceOfBouquet;
    }

    public List<Accessories> getAccessories() {
        return accessories;
    }

    public List<Flower> getFlowers() {
        return flowers;
    }

    private Double countPriceOfFlowers(List<Flower> listOfFlowers) {
        Double totalPrice = 0.0;
        for (Flower flower : listOfFlowers
        ) {
            totalPrice += flower.getPrice();
        }
        return totalPrice;
    }

    private void showFlowers() {
        if (flowers.isEmpty()) {
            System.out.println("There is no flowers in the list");
        } else {
            for (int i = 0; i < flowers.size(); i++) {
                System.out.println(i + " : " + flowers.get(i));
            }
        }
    }

    private void showAccessories() {
        if (accessories.isEmpty()) {
            System.out.println("There is no accessories in the list");
        } else {
            for (int i = 0; i < accessories.size(); i++) {
                System.out.println(i + " : " + accessories.get(i));
            }
        }
    }

    private Double countPriceOfAccessories(List<Accessories> listOfAccessories) {
        Double totalPrice = 0.0;
        for (Accessories accessor : listOfAccessories
        ) {
            totalPrice += accessor.getPrice();
        }
        return totalPrice;
    }

    private List<Flower> addFlowerToBouquet() throws IOException {
        while (true) {
            System.out.println("Enter index of flower you want to add to bouquet:");
            showFlowers();
            int index = Integer.valueOf(READER.readLine());
            if (index >= 0 && index < flowers.size()) {
                Flower flower = flowers.get(index);
                if (flowersOFBouquet.add(flower)) {
                    System.out.println("flower added to the bouquet");
                } else {
                    System.out.println("There is no such option!");
                }
                System.out.println("Do you want to add more flowers to the bouquet?");
                System.out.println("Enter 1 - yes");
                System.out.println("Enter 2 - no");
                int indexOfOtherFlower = Integer.valueOf(READER.readLine());
                switch (indexOfOtherFlower) {
                    case 1:
                        break;
                    case 2:
                        return flowersOFBouquet;
                    default:
                        System.out.println("There is no such option!");
                }
            } else {
                System.out.println("There is no such option!");
            }
        }
    }

    private List<Accessories> addAccessoriesToBouquet() throws IOException {
        while (true) {
            System.out.println("Enter index of accessor you want to add to bouquet:");
            showAccessories();
            int index = Integer.valueOf(READER.readLine());
            if (index >= 0 && index < accessories.size()) {
                Accessories accessor = accessories.get(index);
                if (accessoriesOfBouquet.add(accessor)) {
                    System.out.println("accessor added to the bouquet");
                } else {
                    System.out.println("There is no such option!");
                }
                System.out.println("Do you want to add more accessories to the bouquet?");
                System.out.println("Enter 1 - yes");
                System.out.println("Enter 2 - no");
                int indexOfOtherAccessor = Integer.valueOf(READER.readLine());
                switch (indexOfOtherAccessor) {
                    case 1:
                        break;
                    case 2:
                        return accessoriesOfBouquet;
                    default:
                        System.out.println("There is no such option");
                }
            } else {
                System.out.println("There is no such option!");
            }
        }
    }

    public BouquetOfFlowers createBouquet() throws IOException {
        addFlowerToBouquet();
        addAccessoriesToBouquet();
        Collections.sort(flowersOFBouquet, new ComparatorByFreshness());
        totalPriceOfBouquet = countPriceOfFlowers(flowersOFBouquet) + countPriceOfAccessories(accessoriesOfBouquet);
        BouquetOfFlowers bouquet = new BouquetOfFlowers(flowersOFBouquet, totalPriceOfBouquet, accessoriesOfBouquet);
        System.out.println(bouquet);
        return bouquet;
    }

    private List<Flower> findFlowerByLengthOfStem() throws IOException {
        System.out.println("Enter minimum length of stem:");
        double minLength = Double.valueOf(READER.readLine());
        System.out.println("Enter maximum length of stem:");
        double maxLength = Double.valueOf(READER.readLine());
        List<Flower> flowersWithAppropriateLength = new ArrayList<Flower>();
        for (Flower flower : flowersOFBouquet
        ) {
            if (flower.getLengthOfStem() >= minLength && flower.getLengthOfStem() <= maxLength) {
                flowersWithAppropriateLength.add(flower);
            } else {
                System.out.println("There is no flowers with such length");
            }
        }
        if (!flowersWithAppropriateLength.isEmpty()) {
            for (Flower flower : flowersWithAppropriateLength
            ) {
                System.out.println(flower);
            }
        }
        return flowersWithAppropriateLength;
    }

    public void executeStatement() throws IOException {
        while (true) {
            Menu.showMenu();
            Integer index = Integer.valueOf(READER.readLine());
            Menu menu = Menu.getIndexOfChoice(index);
            switch (menu) {
                case SHOW_FLOWERS:
                    showFlowers();
                    break;
                case SHOW_ACCESSORIES:
                    showAccessories();
                    break;
                case CREATE_BOUQUET:
                    createBouquet();
                    break;
                case FIND_BY_LENGTH:
                    findFlowerByLengthOfStem();
                    break;
                case EXIT:
                    System.out.println("Bye!");
                    return;
                default:
                    System.out.println("There is no such option!");
                    break;
            }
        }
    }
}
