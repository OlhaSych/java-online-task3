package exceptions;

import java.io.IOException;

public class MyAytoCloseable implements AutoCloseable {

    public void close() throws IOException {
        System.out.println("Close");
        throw new IOException("Some text...");
    }

    public void doSomethink() {
        System.out.println("Do something...");
    }
}
