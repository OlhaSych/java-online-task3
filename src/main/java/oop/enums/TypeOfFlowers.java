package oop.enums;

public enum TypeOfFlowers {
    CALENDULA, ROSE, CROCUS, IRIS, LILY, ORCHID, SUNFLOWER, TULIP
}
