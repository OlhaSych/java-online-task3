package oop.classes;

import java.util.List;

public class BouquetOfFlowers extends BouquetOfFlowersAbstract {
    private Double price;
    private List<Accessories> accessories;

    public BouquetOfFlowers(List<Flower> flowers, Double price, List<Accessories> accessories) {
        super(flowers);
        this.price = price;
        this.accessories = accessories;
    }

    public List<Accessories> getAccessories() {
        return accessories;
    }

    public void setAccessories(List<Accessories> accessories) {
        this.accessories = accessories;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    @Override
    public String toString() {
        return "BouquetOfFlowers{" + super.getFlowers() + "\n" +
                "accessories=" + accessories + "\n" +
                "TOTAL PRICE OF BOUQUET : " + price +
                '}';
    }
}
