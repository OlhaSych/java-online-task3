package oop.enums;

public enum ColorOfFlowers {
    WHITE, RED, BLUE, ORANGE, YELLOW, VIOLET
}
