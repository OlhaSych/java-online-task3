package exceptions;

public class UserValidator {
    public boolean validateDateOfUser(User user) throws UserValidationException {
        StringBuilder exceptionMessageBuilder = new StringBuilder();
        if (user.getEmail().length() < 8 || !user.getEmail().contains("@")) {
            exceptionMessageBuilder.append(UserValidationExceptionMessages.BAD_EMAIL.getMessage());
        }
        if (user.getPassword().length() < 6 || user.getPassword().length() > 18) {
            exceptionMessageBuilder.append(UserValidationExceptionMessages.BAD_PASSWORD.getMessage());
        }
        if (exceptionMessageBuilder.length() > 0) {
            throw new UserValidationException(exceptionMessageBuilder.toString());
        }
        return true;
    }
}
