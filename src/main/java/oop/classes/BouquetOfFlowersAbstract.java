package oop.classes;

import java.util.List;

public abstract class BouquetOfFlowersAbstract {
    private List<Flower> flowers;

    public BouquetOfFlowersAbstract(List<Flower> flowers) {
        this.flowers = flowers;
    }

    public List<Flower> getFlowers() {
        return flowers;
    }

    public void setFlowers(List<Flower> flowers) {
        this.flowers = flowers;
    }
 }
