package exceptions;

import java.io.IOException;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        UserValidator validator = new UserValidator();
        System.out.println("Enter email:");
        String email = scanner.next();
        System.out.println("Enter password:");
        String password = scanner.next();
        User user = new User(email, password);
        try {
            validator.validateDateOfUser(user);
        } catch (UserValidationException e) {
            e.printStackTrace();
        }
        testMyAutoCloseable();
    }

    public static void testMyAutoCloseable() {
        try (MyAytoCloseable myAytoCloseable = new MyAytoCloseable()) {
            myAytoCloseable.doSomethink();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
